#include "calledfromqml.h"
#include <QtConcurrent>

CalledFromQml::CalledFromQml(QObject *parent) : QObject(parent)
{
    qDebug()<< "CalledFromQml constructor";
    m_socket.connectToHost("192.168.1.90", 4123);
    m_socket.waitForConnected();
    startTimer(1000);
}

CalledFromQml::~CalledFromQml()
{
    m_socket.disconnect();
    m_socket.waitForDisconnected();
    m_socket.close();
}

void CalledFromQml::fromQml(int value)
{
    qDebug()<< "sent to socket " << value;
    char dataToBeWritten[2];
    dataToBeWritten[0]= value;
    dataToBeWritten[1]= 0;
    writeToSocket(&dataToBeWritten[0], binary);
}

void CalledFromQml::timerEvent(QTimerEvent *event)
{
    char dataToBeWritten[3];
    dataToBeWritten[0]= 'k';
    dataToBeWritten[1]= 'a';
    dataToBeWritten[2]= 0;
    qDebug() << "keepalive sent!";
    writeToSocket(&dataToBeWritten[0], ascii);
}

void CalledFromQml::writeToSocket(const char* data, SentDataType dataType) {
    int i=0;
    while ( data[i] != 0 ) {
        i++;
    }
    u_int8_t dataReceived[i+1];
    m_socket.write( data );
    timer.start();
    m_socket.waitForBytesWritten();
    m_socket.waitForReadyRead();
    m_socket.read((char*)&dataReceived, i);
    timer.elapsed();
    dataReceived[i+1] = 0;
    if ( dataType == ascii )
        qDebug() << "received from socket " << (char*)dataReceived << " turnaround time " << timer.elapsed();
    else
        qDebug() << "received from socket " << dataReceived[0] << " turnaround time " << timer.elapsed();
}

#ifndef CALLEDFROMQML_H
#define CALLEDFROMQML_H

#include <QObject>
#include <QtNetwork>

class CalledFromQml : public QObject
{
    Q_OBJECT
public:
    enum SentDataType { binary, ascii };
    explicit CalledFromQml(QObject *parent = 0);
    ~CalledFromQml();


    void keepAliveSocket(int value);
    void timerEvent(QTimerEvent *event);
signals:

public slots:
    void fromQml(int value);
private:
    void writeToSocket(const char *data, SentDataType dataType);
    QTcpSocket m_socket;
    QElapsedTimer timer;
};

#endif // CALLEDFROMQML_H

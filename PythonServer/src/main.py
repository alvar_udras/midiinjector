import socket
import rtmidi
from asyncio.tasks import sleep
from pip._vendor.distlib.compat import raw_input
from threading import Thread
from rtmidi._rtmidi import MidiMessage

TCP_IP = '192.168.1.90'
TCP_PORT = 4123
inputPort = 3
outputPort = 4

midiin = rtmidi.RtMidiIn()
midiout = rtmidi.RtMidiOut()

def startServer():
    print ('starting to listen to server:')    
    BUFFER_SIZE = 2  # Normally 1024, but we want fast response    
        
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)       
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)
    
    conn, addr = s.accept()
    print ('Connection address:', addr)
    while 1:
        data = conn.recv(BUFFER_SIZE)
        print ("raw data receivedd ", data)
        if ( len(data) > 1 ):
            #receive last byte of keepalive
            conn.send(data)
            continue        
        value = data[0]
        if not data: break
        print ("received data: %d" % value)
        midiout.sendMessage(MidiMessage.controllerEvent(1,75, value))
        conn.send(data)
    print ("quitting app")
    conn.close()

def midiEventHandler(midiEvent):
    if ( midiEvent is not None ):
        print(midiEvent)
        midiout.sendMessage(midiEvent)    

def initMidi():
    print('input ports:')    

    for i in range( 0, midiin.getPortCount() ):
        print(midiin.getPortName(i))
    print ('output ports:')
    for i in range(0, midiout.getPortCount()):
        print (midiout.getPortName(i))
    midiin.openPort(3)
    midiout.openPort(2)
    midiin.setCallback(midiEventHandler)

    
    


if __name__ == "__main__":    
    Thread(target = startServer).start()
    Thread(target = initMidi).start()
    raw_input('select action:')